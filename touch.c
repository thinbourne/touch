#include <stdio.h>
#include <poll.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>

int init_gpio(void){
	int exp_fd, value_fd, dir_fd, poll_edge, ret;
	char rising[10];

	sprintf(rising, "rising");
	exp_fd = open("/sys/class/gpio/export", O_WRONLY);
	if(exp_fd < 0){
		printf("EXPORT OPEN ERROR\n");
		return -1;
	}
	ret = write(exp_fd, "30", 2);
	if(ret < 0){
		printf("EXPORT ERROR\n");
		return -1;
	}
	ret = write(exp_fd, "15", 2);
	if(ret < 0){
		printf("EXPORT ERROR\n");
		return -1;
	}
	dir_fd = open("/sys/class/gpio/gpio30/direction", O_WRONLY);
	write(dir_fd, "out", 3);
	value_fd = open("/sys/class/gpio/gpio30/value", O_WRONLY);
	if(value_fd < 0){
		printf("VALUE GPIO ERROR\n");
		return -1;
	}
	ret = write(value_fd, "0", 1);
	if(ret < 0){
		printf("VALUE WRITE ERROR\n");
		return -1;
	}
	dir_fd = open("/sys/class/gpio/gpio15/direction", O_WRONLY);
	if(dir_fd < 0){
		printf("GPIO ERROR\n");
		return -1;
	}
	ret = write(dir_fd, "in", 2);
	if(ret < 0){
		printf("GPIO DIRECTION WRITE ERROR\n");
		return -1;
	}
	poll_edge = open("/sys/class/gpio/gpio15/edge", O_WRONLY);
	if(poll_edge < 0){
		printf("POLL EDGE ERROR\n");
		return -1;
	}
	ret = write(poll_edge, rising, strlen(rising));
	if(ret < 0){
		printf("POLL EDGE WRITE ERROR\n");
		return -1;
	}
	close(value_fd);
	close(exp_fd);
	close(dir_fd);
	close(poll_edge);
	return 0;
}	

void exit_gpio(void){
	int gpio_ue_fd;
	
	gpio_ue_fd = open("/sys/class/gpio/unexport", O_WRONLY);
	write(gpio_ue_fd, "15", 3);
	write(gpio_ue_fd, "30", 3);
	close(gpio_ue_fd);
}

int main(){
	int ret, poll_value, number = 0;
	struct pollfd poll_sensor;
	char buf[10];

	printf("BEFORE GPIO INIT\n");	
	ret = init_gpio();
	if(ret < 0){
		printf("GPIO MAIN ERROR\n");
		exit_gpio();
		return -1;
	}
	poll_value = open("/sys/class/gpio/gpio15/value", O_RDONLY);
	if(poll_value < 0){
		exit_gpio();
		return -1;
	}
	printf("BEFORE WHILE\n");
	while(number < 20){
		memset((void*)&poll_sensor, 0, sizeof(struct pollfd));
		poll_sensor.fd		= poll_value;
		poll_sensor.events	= POLLPRI;
		poll_sensor.revents	= 0;
		ret = poll(&poll_sensor, 1, -1);
		if(ret > 0){
			if(poll_sensor.revents & POLLPRI){
				read(poll_value, buf, sizeof(buf));
				printf("SENSOR TOUCHED\n");
				number++;
			}
		}
	printf("Number: %d\n", number);
	}
	printf("AFTER WHILE\n");
	close(poll_value);
	exit_gpio();
	printf("EXITING\n");
	return 0;
}
